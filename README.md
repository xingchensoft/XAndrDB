#XAndrDB
关于编写这个框架的初衷，为的就是能在安卓中使用对象一样操作数据库，
但是一想操作数据库SQLite使用起来这么麻烦。所以结合以前的经验再加上阅读mybatis的源码。
自己写了一个'安卓端的mybatis'
##使用说明：
在使用本框架之前首先
将本项目根目录下的XAndrDB.jar引入到你的项目中，然后。
在你的app的AndroidManifest.xml中配置：

	<application
	android:name="com.xcode.xandrdb.Session.SessionApplication"
	……
	>
####这时候就该考虑使用问题了。
不多说，直接贴代码：

	package com.xcode.xandrdb.mapper;
	
	import com.xcode.xandrdb.User;
	import com.xcode.xandrdb.annotation.Create;
	import com.xcode.xandrdb.annotation.Delete;
	import com.xcode.xandrdb.annotation.Insert;
	import com.xcode.xandrdb.annotation.Select;
	import com.xcode.xandrdb.annotation.Update;
	import com.xcode.xandrdb.annotation.name;
	
	public interface test
	{
		//插入的操作
		@Insert("INSERT INTO xiaolei ('sname', 'snumber') VALUES ('xiaolei', 'xiaolei')")
		public String insert();
		
		//删除操作
		@Delete("delete from xiaolei where id = #{id}")
		public String delete(@name("id")int id);
		
		//更新操作
		@Update("UPDATE xiaolei SET sname='xiaolei2', snumber='xiaolei2' WHERE (_id=#{id})")
		public String update(@name("id")int id);
		
		//查询操作
		@Select("SELECT * FROM xiaolei WHERE _id = #{id}")
		public User[] select(@name("id")int id);
		
		//新建操作
		@Create("create table #{user.name}(_id integer primary key autoincrement,sname text,snumber text)")
		public String create(@name("user")User user);
	}
这里只是定义了一系列的数据库操作action的Mapper。那我们看看怎么使用这些Mapper：

		Session session = SessionFactory.getSession(new SessionConfig().setDBName("xiaolei"));
		test t1 = session.getMapper(test.class);
		User users[] = t1.select(1);
		System.out.println(users);

是的，所有的数据库操作都必须使用Session对象，去操作。然后使用Session对象拿到mapper，内部通过动态代理操作返回你一个mapper对象，然后你操作你的mapper里面定义的方法就是在操作数据库了。
#####这是一个完全面向切面，使用 自定义注解+反射+动态代理 结合在一起发挥强大功能的数据库框架。


##关于为什么我还是依然在框架中保留sql语句，为什么不直接屏蔽sql语句，完全以操作对象的方式操作数据库呢？
### 1.第一点，是我的功力还没有那么深，还没有能力做到这一步。
### 2.第二点，我觉得数据库sql语句变化多端，太过高深，用纯对象去尝试操作的话，可能会弄巧成拙。譬如Hibernate。屏蔽了一系列规则，却为了填坑又自定义自己的规则。感觉还是没什么必要。







##曾经..
	曾经有一份至真嘅爱情摆喺我面前，
	但我冇去珍惜，
	到冇咗嘅时候先至后悔莫及，
	尘世间最痛苦莫过于此。
	如果个天可以畀个机会我返转头嘅话，我会同个女仔讲我爱佢！
	如果 系都要喺呢份爱加上一个期限，
	我希望系一万年！
